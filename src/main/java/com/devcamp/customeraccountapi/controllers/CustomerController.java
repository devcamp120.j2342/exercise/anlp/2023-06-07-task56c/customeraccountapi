package com.devcamp.customeraccountapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customeraccountapi.models.Customer;
import com.devcamp.customeraccountapi.services.CustomerService;

import org.springframework.web.bind.annotation.GetMapping;


@RestController
@RequestMapping("/api")
@CrossOrigin
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomers() {
        return customerService.getAllCustomers();
    }
    
}
