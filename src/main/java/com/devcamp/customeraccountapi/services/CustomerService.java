package com.devcamp.customeraccountapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customeraccountapi.models.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer(1, "An", 0);
    Customer customer2 = new Customer(2, "Minh", 10);
    Customer customer3 = new Customer(3, "Hieu", 5);

    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> allCustomers = new ArrayList<>();
        allCustomers.add(customer1);
        allCustomers.add(customer2);
        allCustomers.add(customer3);
        return allCustomers;
    }
}
